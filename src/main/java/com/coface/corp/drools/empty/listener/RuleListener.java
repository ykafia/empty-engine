
package com.coface.corp.drools.empty.listener;

import java.util.Iterator;

import org.kie.api.event.KieRuntimeEvent;
import org.kie.api.event.rule.BeforeMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.event.rule.RuleFlowGroupActivatedEvent;
import org.kie.api.runtime.process.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A tool to log rule executions in the process.
 */
public class RuleListener extends DefaultAgendaEventListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(RuleListener.class);

	@Override
	public void beforeMatchFired(BeforeMatchFiredEvent event) {
		getLogger(event).debug("=> Rule Fired : '{}'", event.getMatch().getRule().getName());

	}

	@Override
	public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
		getLogger(event).debug("=> RuleFlowGroup Activated : '{}'", event.getRuleFlowGroup().getName());
	}

	private Logger getLogger(KieRuntimeEvent event) {
		try {

			Iterator<ProcessInstance> itr = event.getKieRuntime().getProcessInstances().iterator();
			ProcessInstance lastElement = itr.next();

			while (itr.hasNext()) {
				lastElement = itr.next();
			}
			return LoggerFactory.getLogger(lastElement.getProcessId());

		} catch (Exception e) {
			return LOGGER;
		}
	}
}
