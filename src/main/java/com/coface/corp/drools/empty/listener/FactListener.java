package com.coface.corp.drools.empty.listener;

import java.util.Iterator;

import org.kie.api.event.KieRuntimeEvent;
import org.kie.api.event.rule.ObjectDeletedEvent;
import org.kie.api.event.rule.ObjectInsertedEvent;
import org.kie.api.event.rule.ObjectUpdatedEvent;
import org.kie.api.event.rule.RuleRuntimeEventListener;
import org.kie.api.runtime.process.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A tool to log facts manipulation in the Working Memory.
 */
public class FactListener implements RuleRuntimeEventListener {

	Logger LOGGER = LoggerFactory.getLogger(FactListener.class);

	@Override
	public void objectInserted(ObjectInsertedEvent event) {

		String byRule = (event.getRule() != null) ? "by Rule" : "from Outside";

		getLogger(event).debug("=> Fact Inserted {} : '{}' '{}'", byRule, event.getObject().getClass().getSimpleName(),
				event.getObject().toString());
	}

	@Override
	public void objectUpdated(ObjectUpdatedEvent event) {
		String byRule = (event.getRule() != null) ? "by Rule" : "from Outside";

		getLogger(event).debug("=> Fact Updated {} : {} {}", byRule, event.getObject().getClass().getSimpleName(),
				event.getObject().toString());
	}

	@Override
	public void objectDeleted(ObjectDeletedEvent event) {
		String byRule = (event.getRule() != null) ? "by Rule" : "from Outside";

		getLogger(event).debug("=> Fact Deleted {} : {} {}", byRule, event.getOldObject().getClass().getSimpleName(),
				event.getOldObject().toString());

	}

	private Logger getLogger(KieRuntimeEvent event) {
		try {

			Iterator<ProcessInstance> itr = event.getKieRuntime().getProcessInstances().iterator();
			ProcessInstance lastElement = itr.next();

			while (itr.hasNext()) {
				lastElement = itr.next();
			}
			return LoggerFactory.getLogger(lastElement.getProcessId());

		} catch (Exception e) {
			return LOGGER;
		}
	}
}